import React, { useState } from 'react';
import Button from "./components/Button";
import Modal from "./components/Modal";


const App = () => {
    const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
    const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

    const openFirstModal = () => {
        setIsFirstModalOpen(true);
    };

    const openSecondModal = () => {
        setIsSecondModalOpen(true);
    };

    const closeFirstModal = () => {
        setIsFirstModalOpen(false);
    };

    const closeSecondModal = () => {
        setIsSecondModalOpen(false);
    };

    return (
        <div>
            <Button backgroundColor="LightBlue" text="Open first modal" onClick={openFirstModal} />
            <Button backgroundColor="HotPink" text="Open second modal" onClick={openSecondModal} />

            {isFirstModalOpen && (
                <Modal
                    backgroundModal="LightBlue"
                    header="Would you like to submit the form?"
                    closeButton={true}
                    text="Ensure all the information is accurate before proceeding. Confirm your submission."
                    actions={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Cancel" onClick={closeFirstModal} />}
                    action={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Submit" onClick={closeFirstModal} />}
                    onClose={closeFirstModal}
                />
            )}

            {isSecondModalOpen && (
                <Modal
                    backgroundModal="HotPink"
                    header="Are you sure you want to discard changes?"
                    closeButton={true}
                    text="Any unsaved edits will be lost. Confirm to discard changes."
                    actions={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Cancel" onClick={closeSecondModal} />}
                    action={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Discard" onClick={closeSecondModal} />}
                    onClose={closeSecondModal}
                />
            )}
        </div>
    );
};


export default App;
