import React from 'react';
import "../styles/button.scss"

const Button = ({ backgroundColor, text, onClick }) => {
    return (
        <button className="button-styled" style={{ backgroundColor }} onClick={onClick}>
            {text}
        </button>
    );
};

export default Button;
