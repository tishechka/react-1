import React from "react";
import "../styles/modal.scss"

const Modal = ({header, closeButton, text, actions, action, onClose, backgroundModal}) => {
    const styleModal = {
        background: backgroundModal
    }

    return (
        <div className="modal-overlay" onClick={onClose}>
            <div className="modal" style={styleModal}>
                <div className="modal-header">
                    <h2>{header}</h2>
                    {closeButton && <span className="close-button" onClick={onClose}>&times;</span>}
                </div>
                <p className="modal-txt">{text}</p>
                <div className="modal-actions">{action} {actions}</div>
            </div>
        </div>
    );
};

export default Modal;
